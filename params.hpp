
EXT double g_Lx, g_Ly, g_Lz, g_Rho;
EXT unsigned int g_N;
EXT double g_BinWid;
EXT unsigned int g_NBins;
EXT double g_sigma;
EXT double * g_x, * g_y, * g_z;
EXT unsigned int g_cl_dev, g_tilesize;
