# include <CL/cl.h>

#ifdef __cplusplus
extern "C" {
#endif

    cl_program * ocl_create_programs(char **, cl_uint, cl_uint *, cl_context **, cl_device_id **);

#ifdef __cplusplus
}
#endif
