
/*  This is an OpenCL kernel function that takes the particle co-ordinates
 *  of a monoatomic system, and computes the pair-correlation histogram, 
 *  which would have to be normalized appropriately to get the pair correlation.
 * 
 *  Copyright (C) 2020  Arunkumar Bupathy (arunbupathy@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

# pragma OPENCL EXTENSION cl_khr_fp64: enable
# pragma OPENCL EXTENSION cl_khr_int64_base_atomics: enable

////////////////////////////////////////////////////////////////////////////////

__kernel void ocl_pair_corr(__global ulong * d_corr, __global double * d_coord, __constant double Ls[4], unsigned int N, __local double * d_x, __local double * d_y, __local double * d_z)
{
    size_t block_x = get_group_id(0); // the x 
    size_t block_y = get_group_id(1); // and y co-ordinates of the block we're working on
    
    size_t lsize = get_local_size(0); // the length of the workgroup in the x direction
    // the length of the workgroup in the y direction is 2
    
    if(block_x >= block_y) // only proceed if upper triangle or diagonal
    {
        size_t thread_i = get_local_id(0); // i and j co-ordinates of the current thread within its thread group
        size_t thread_j = get_local_id(1);
        
        size_t block_xy[2] = {block_x, block_y};
        // block_xy[thread_j] tells that if thread_j == 0 the given thread should
        // load the co-ordinates corresponding to iterations of the outer sum
        // and if thread_j == 1 it should load those of the inner sum
        
        // this is the index of the particle whose co-ordinates are to be loaded locally
        size_t index = block_xy[thread_j] * lsize + thread_i;
        
        if(index < N) // only load the element if the index is < N
        {
            d_x[thread_j * lsize + thread_i] = d_coord[index]; // load the co-ordinates from global to local memory
            d_y[thread_j * lsize + thread_i] = d_coord[index + N]; // the y co-ordinate
            d_z[thread_j * lsize + thread_i] = d_coord[index + 2 * N]; // the z co-ordinate
        }
        barrier(CLK_LOCAL_MEM_FENCE); // this is very important to make sure that all threads have loaded
        
        // Note that we only perform O(n) global memory fetches for (n x n) particle pairs
        // this reduces global memory access which is the limiting factor for achieving peak compute
        
        // now our thread group is (lsize x 2) in size
        // but the pair-correlation block is (lsize x lsize) in size
        // so we process the blocks in 'strips' of (lsize x 2) at a time
        // there are (lsize / 2) such strips
        for(size_t strip = 0; strip < lsize / 2; ++strip)
        {
            size_t sub_j = thread_j + strip * 2;
            size_t sub_i = thread_i;
            
            size_t ple_i = block_x * lsize + sub_i; // these are the actual particle indices
            size_t ple_j = block_y * lsize + sub_j; // whose pair correlation is being computed
            
            if(ple_i < N && ple_j < ple_i) // only compute pair correlations for the upper triangle
            {
                double diffX = d_x[sub_i] - d_x[lsize + sub_j];
                double rX = diffX - Ls[0] * round(diffX / Ls[0]);
                
                double diffY = d_y[sub_i] - d_y[lsize + sub_j];
                double rY = diffY - Ls[1] * round(diffY / Ls[1]);
                
                double diffZ = d_z[sub_i] - d_z[lsize + sub_j];
                double rZ = diffZ - Ls[2] * round(diffZ / Ls[2]);
                
                double rsq = rX * rX + rY * rY + rZ * rZ;
                double rij_scaled = sqrt(rsq) / Ls[3];
                
                ptrdiff_t icntr = round(rij_scaled);
                
                atom_inc((volatile __global ulong *) d_corr + icntr); // atom_inc on an integer buffer since we're only getting the histogram // this needs to be normalized
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
