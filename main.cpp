/*  A simple g(r) code using OpenCL. This is the main program.
 * 
 *  Author: Arunkumar Bupathy
 *  email: arunbupathy@gmail.com
 *  July 2020
 * 
 *  This software is distributed under the GNU GPLv3
 *  See www.gnu.org for the licence terms */

////////////////////////////////////////////////////////////////////////////////

# include <cstdlib>
# include <cstdint>

# define EXT ;
# include "params.hpp"
# undef EXT
# include "helper.hpp"
# include "ocl_pair_corr.hpp"

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
    g_cl_dev = 0; // the default OpenCL device
    g_tilesize = 32; // the default tile size
    g_BinWid = 0.02; // a reasonable default
    
    int state = get_params(argc, argv); // get input and output file names
    
    if(state != 0) return 1; // means the infile could not be read
    
    g_x = new double [g_N] (); // x coord
    g_y = new double [g_N] (); // y coord
    g_z = new double [g_N] (); // z coord
    unsigned long long * corr = new unsigned long long [g_NBins] (); // pair correlation function
    
    read_file(); // read the co-ordinates into g_x, g_y and g_z
    
    get_pair_corr(corr, g_x, g_y, g_z, g_Lx, g_Ly, g_Lz, g_N, g_BinWid * g_sigma, g_NBins, g_tilesize);
    
    delete[] g_x; // free the memory
    delete[] g_y;
    delete[] g_z;
    
    store_pair_corr(corr); // store the pair correlations (after properly normalizing the integer "corr" histogram)
    
    delete[] corr; // free the memory
    
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
