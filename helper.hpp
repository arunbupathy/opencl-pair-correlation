
int get_params(int, char**);

void read_file(void);

void store_pair_corr(unsigned long long *);

struct timespec time_diff(struct timespec, struct timespec);

void current_utc_time(struct timespec *);
