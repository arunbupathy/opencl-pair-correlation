
# define CL_TARGET_OPENCL_VERSION 120

# include <iostream>
# include <cstdlib>
# include <cmath>
# include <CL/cl.h>

# define EXT extern
# include "params.hpp"
# undef EXT
# include "ocl_create_programs.h"
# include "helper.hpp"

////////////////////////////////////////////////////////////////////////////////

void get_pair_corr(unsigned long long * corr, double * x, double * y, double * z, double Lx, double Ly, double Lz, unsigned int N, double BinWid, unsigned int nBins, size_t lsize)
{
    char * cl_source = (char *) "ocl_pair_corr.cl";
    cl_int cl_status;
    cl_uint num_cl_dev;
    cl_context * contexts;
    cl_device_id * devices;
    
    cl_program * programs = ocl_create_programs(&cl_source, 1, &num_cl_dev, &contexts, &devices);
    
    cl_command_queue queue = clCreateCommandQueue(contexts[g_cl_dev], devices[g_cl_dev], 0, NULL);
    
    struct timespec tstart, tend;
    current_utc_time(&tstart);
    
    cl_mem d_coord = clCreateBuffer(contexts[g_cl_dev], CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, sizeof(double) * N * 3, NULL, &cl_status); // single buffer for the three co-ordinates (3N)
    
    cl_status = clEnqueueWriteBuffer(queue, d_coord, CL_TRUE, 0, sizeof(double) * N, x, 0, NULL, NULL);
    cl_status = clEnqueueWriteBuffer(queue, d_coord, CL_TRUE, sizeof(double) * N, sizeof(double) * N, y, 0, NULL, NULL);
    cl_status = clEnqueueWriteBuffer(queue, d_coord, CL_TRUE, sizeof(double) * N * 2, sizeof(double) * N, z, 0, NULL, NULL);
    
    cl_mem d_corr = clCreateBuffer(contexts[g_cl_dev], CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(unsigned long long) * nBins, corr, &cl_status); // buffer for the pair correlation
    
    double Ls[4] = {Lx, Ly, Lz, BinWid};
    cl_mem d_Ls = clCreateBuffer(contexts[g_cl_dev], CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, sizeof(double) * 4, Ls, &cl_status);
    
    cl_kernel kn_ocl_pair_corr = clCreateKernel(programs[g_cl_dev], "ocl_pair_corr", &cl_status);
    
    cl_status = clSetKernelArg(kn_ocl_pair_corr, 0, sizeof(cl_mem), &d_corr);
    cl_status = clSetKernelArg(kn_ocl_pair_corr, 1, sizeof(cl_mem), &d_coord);
    cl_status = clSetKernelArg(kn_ocl_pair_corr, 2, sizeof(cl_mem), &d_Ls);
    cl_status = clSetKernelArg(kn_ocl_pair_corr, 3, sizeof(unsigned int), &N); // simple constants can be directly set
    cl_status = clSetKernelArg(kn_ocl_pair_corr, 4, 2 * lsize * sizeof(double), NULL);
    cl_status = clSetKernelArg(kn_ocl_pair_corr, 5, 2 * lsize * sizeof(double), NULL);
    cl_status = clSetKernelArg(kn_ocl_pair_corr, 6, 2 * lsize * sizeof(double), NULL);
    
    size_t blockLen = lsize; // operate on (lsize x lsize) blocks of pair computations
    size_t clGroupSize[2] = {lsize, 2}; // using (lsize x 2) threads per block
    
    size_t numBlocks = ceil(((double) N) / ((double) blockLen));
    // divide the input vector into numBlock blocks, so each block is blockLen wide; ceil is to pad the last block
    size_t clWorkSize[2] = {numBlocks * lsize, numBlocks * 2};
    // note that this is the total number of threads to be created along each dimension, not the number of blocks
    // the runtime computes the number of blocks along 'dim' as WorkSize[dim]/GroupSize[dim]
    
    cl_status = clEnqueueNDRangeKernel(queue, kn_ocl_pair_corr, 2, NULL, clWorkSize, clGroupSize, 0, NULL, NULL);
    
    cl_status = clEnqueueReadBuffer(queue, d_corr, CL_TRUE, 0, sizeof(unsigned long long) * nBins, corr, 0, NULL, NULL); // pair correlation function
    
    current_utc_time(&tend);
    struct timespec tdiff = time_diff(tstart, tend);
    
    std::cout << "Time taken for the computations including buffer creation and data transfer: " << ((tdiff.tv_sec * 1000000000 + tdiff.tv_nsec) / 1000000000.0) << "s." << std::endl;
    
    clReleaseKernel(kn_ocl_pair_corr);
    
    clReleaseMemObject(d_corr); // free the memory
    clReleaseMemObject(d_coord);
    clReleaseMemObject(d_Ls);
    
    clReleaseCommandQueue(queue);

    for(cl_uint icldev = 0; icldev < num_cl_dev; ++icldev)
    {
        clReleaseProgram(programs[icldev]);
        clReleaseContext(contexts[icldev]);
    }
    
    free(programs);
    free(devices);
    free(contexts);
}

////////////////////////////////////////////////////////////////////////////////
