
/* This is a helper program to find OpenCL devices, create contexts,
 * and compile the given OpenCL source codes into program objects.
 * 
 *  Copyright (C) 2020  Arunkumar Bupathy (arunbupathy@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

# define CL_TARGET_OPENCL_VERSION 120

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <CL/cl.h>

char * load_cl_source(char * file, size_t * length)
{
    FILE * fp;
    char * buffer;

    fp = fopen(file, "r");
    if(!fp)
        exit(1);
    
    fseek(fp, 0L, SEEK_END);
    *length = ftell(fp);
    rewind(fp);

    buffer = calloc(sizeof(char), *length);
    /* copy the file into the buffer */
    int nread = fread(buffer, sizeof(char), *length, fp);
    
    fclose(fp);
    
    if(nread != *length)
        exit(1);
    
    return buffer;
}

cl_program * ocl_create_programs(char * cl_source[], cl_uint num_sources, cl_uint * total_devices, cl_context ** contexts, cl_device_id ** devices)
{
    cl_uint num_platforms;
    cl_int status = clGetPlatformIDs(0, NULL, &num_platforms);
    if(status != CL_SUCCESS || num_platforms == 0)
        return NULL;
    
    cl_platform_id * platforms = (cl_platform_id*) malloc(num_platforms * sizeof(cl_platform_id));
    clGetPlatformIDs(num_platforms, platforms, NULL);
    
    cl_uint num_devices[num_platforms];
    cl_device_id * devices_temp[num_platforms];
    *total_devices = 0; // important to initialize this to zero!
    for(cl_uint iplat = 0; iplat < num_platforms; ++iplat)
    {
        status = clGetDeviceIDs(platforms[iplat], CL_DEVICE_TYPE_GPU, 0, NULL, &num_devices[iplat]);
        if(status != CL_SUCCESS || num_devices[iplat] == 0)
            num_devices[iplat] = 0;
        else
        {
            devices_temp[iplat] = (cl_device_id*) malloc(num_devices[iplat] * sizeof(cl_device_id));
            status = clGetDeviceIDs(platforms[iplat], CL_DEVICE_TYPE_GPU, num_devices[iplat], devices_temp[iplat], NULL);
            *total_devices += num_devices[iplat];
        }
    }
    free(platforms);
    
    size_t cl_source_length[num_sources];
    char * cl_prog_source[num_sources];
    for(cl_uint isource = 0; isource < num_sources; ++isource)
        cl_prog_source[isource] = load_cl_source(cl_source[isource], &cl_source_length[isource]);
    
    *contexts = (cl_context*) malloc(sizeof(cl_context) * (*total_devices));
    *devices = (cl_device_id*) malloc(sizeof(cl_device_id) * (*total_devices));
    cl_program * programs = (cl_program*) malloc(sizeof(cl_program) * (*total_devices));
    
    for(cl_uint iplat = 0, idev = 0; iplat < num_platforms; ++iplat)
    {
        if(num_devices[iplat] > 0)
        {
            for(int inumdev = 0; inumdev < num_devices[iplat]; ++inumdev)
            {
                (*devices)[idev] = devices_temp[iplat][inumdev];
                (*contexts)[idev] = clCreateContext(NULL, 1, devices_temp[iplat] + inumdev, NULL, NULL, &status); // create a context with the chosen device
                programs[idev] = clCreateProgramWithSource((*contexts)[idev], num_sources, (const char **) cl_prog_source, (const size_t *) cl_source_length, &status);
                status = clBuildProgram(programs[idev], 1, devices_temp[iplat] + inumdev, "-cl-std=CL1.2", NULL, NULL);
                if(status != CL_SUCCESS)
                {
                    printf("Program build for device %d was not successful.\n", idev);
                    size_t buildloglength = 0;
                    status = clGetProgramBuildInfo(programs[idev], (*devices)[idev], CL_PROGRAM_BUILD_LOG, 0, NULL, &buildloglength);
                    char * buildlog = (char *) calloc(sizeof(char), buildloglength/sizeof(char));
                    status = clGetProgramBuildInfo(programs[idev], (*devices)[idev], CL_PROGRAM_BUILD_LOG, buildloglength, buildlog, NULL);
                    for(int i0 = 0; i0 < buildloglength; ++i0)
                        printf("%c", buildlog[i0]);
                    free(buildlog);
                }
                idev++;
            }
            free(devices_temp[iplat]);
        }
    }
    
    for(cl_uint isource = 0; isource < num_sources; ++isource)
        free(cl_prog_source[isource]);
    
    return programs;
}
