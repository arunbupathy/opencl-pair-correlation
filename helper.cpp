# include <iostream>
# include <cstdlib>
# include <string>
# include <fstream>
# include <cmath>

# define EXT extern
# include "params.hpp"
# undef EXT

std::string g_infile = "unspec";
std::string g_outfile = "gofr";

////////////////////////////////////////////////////////////////////////////////

int hashit (std::string const& inString)
{
    if (inString == "-in") return 1;
    if (inString == "-out") return 2;
    if (inString == "-cldev") return 3;
    if (inString == "-tilesize") return 4;
    return 0;
}

////////////////////////////////////////////////////////////////////////////////

int get_params(int argc, char* argv[])
{
    for(int i = 1; i < argc; i+=2)
    {
        switch(hashit(std::string(argv[i])))
        {
            case 1: g_infile = std::string(argv[i+1]); break;
            case 2: g_outfile = std::string(argv[i+1]); break;
            case 3: g_cl_dev = std::stoi(argv[i+1]); break;
            case 4: g_tilesize = std::stoi(argv[i+1]); break;
            default: break;
        }
    }
    
    if(g_infile == "unspec")
    {
        std::cerr << "Please specify the input file (-in <infile>) and try again." << std::endl;
        return 1;
    }
    
    std::ifstream infile(g_infile.c_str());
    if(!infile.good())
    {
        std::cerr << "There was an error reading the file " << g_infile << std::endl;
        return 1;
    }
    
    std::string inword;
    infile >> inword;
    g_Lx = -std::stod(inword);
    infile >> inword;
    g_Lx += std::stod(inword);
    infile >> inword;
    g_Ly = -std::stod(inword);
    infile >> inword;
    g_Ly += std::stod(inword);
    infile >> inword;
    g_Lz = -std::stod(inword);
    infile >> inword;
    g_Lz += std::stod(inword);
    infile >> inword;
    g_sigma = std::stod(inword);
    
    g_NBins = ceil( (sqrt(g_Lx * g_Lx + g_Ly * g_Ly + g_Lz * g_Lz) / 2.0 / g_sigma) / g_BinWid);
    
    g_N = 0;
    while(true)
    {
        infile >> inword;
        if(infile.eof()) break;
        infile >> inword;
        infile >> inword;
        g_N++;
    }
    
    infile.close();
    
    g_Rho = (g_N * 1.0) / g_Lx / g_Ly / g_Lz;
    
    return 0;
}

////////////////////////////////////////////////////////////////////////////////

void read_file(void)
{
    std::ifstream infile(g_infile.c_str());
    std::string inword;
    
    for(int i0 = 0; i0 < 7; i0++)
        infile >> inword;
    
    int i0 = 0;
    infile >> inword;
    while(!infile.eof())
    {
        g_x[i0] = std::stod(inword);
        infile >> inword;
        g_y[i0] = std::stod(inword);
        infile >> inword;
        g_z[i0++] = std::stod(inword);
        infile >> inword;
    }    
    infile.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void store_pair_corr(unsigned long long * corr)
{
    std::ifstream ifile(g_outfile.c_str());
    if(!ifile.is_open())
    {
        std::ofstream ofile(g_outfile.c_str());
        ofile << "# r\t g(r)" << std::endl;
        ofile.close();
    }
    else
    {
        ifile.close();
    }
    
    std::ofstream corrfile(g_outfile.c_str(), std::ofstream::out|std::ofstream::app);
    
    corrfile << (0.0) << "\t" << (0.0) << std::endl;
    
    for(unsigned int i = 1; i < g_NBins; i++)
    {
        double rad = i * g_BinWid * g_sigma;
        
        if(rad >= g_Lx / 2.0) continue; // important condition to check
        
        double orad = (i + 0.5) * g_BinWid * g_sigma;
        double ovol = 4.0 * M_PI * orad * orad * orad / 3.0;
        
        double irad = (i - 0.5) * g_BinWid * g_sigma;
        double ivol = 4.0 * M_PI * irad * irad * irad / 3.0;
        
        double dvol = fabs(ovol - ivol);
        
        double gr = corr[i] * 2.0 / (dvol * g_Rho * g_N);
        
        corrfile << rad << "\t" << gr << std::endl;
    }
    corrfile.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
 
/* 
author: jbenet
os x, compile with: gcc 
linux, compile with: gcc -lrt
*/
  
#include <time.h>
#include <sys/time.h>
#include <stdio.h>
  
#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif
  
  
void current_utc_time(struct timespec *ts) {
  
#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
  clock_serv_t cclock;
  mach_timespec_t mts;
  host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
  clock_get_time(cclock, &mts);
  mach_port_deallocate(mach_task_self(), cclock);
  ts->tv_sec = mts.tv_sec;
  ts->tv_nsec = mts.tv_nsec;
#else
  clock_gettime(CLOCK_REALTIME, ts);
#endif
  
}
  
/* 
int main(int argc, char **argv) {
  
  struct timespec ts;
  current_utc_time(&ts);
  
  printf("s:  %lu\n", ts.tv_sec);
  printf("ns: %lu\n", ts.tv_nsec);
  return 0;
  
}*/
 
////////////////////////////////////////////////////////////////////////////////
 
/* 
author: Guy Rutenberg
os x, compile with: gcc 
linux, compile with: gcc -lrt
*/
  
struct timespec time_diff(struct timespec start, struct timespec end)
{
  timespec temp;
  if ((end.tv_nsec-start.tv_nsec)<0) {
    temp.tv_sec = end.tv_sec-start.tv_sec-1;
    temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
  } else {
    temp.tv_sec = end.tv_sec-start.tv_sec;
    temp.tv_nsec = end.tv_nsec-start.tv_nsec;
  }
  return temp;
}
 
////////////////////////////////////////////////////////////////////////////////
