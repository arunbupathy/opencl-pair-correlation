# OpenCL Pair Correlation

## A standalone program that computes the pair correlation function of a 3D mono-atomic system using OpenCL on the GPU

An OpenCL based pair correlation function implementation that is reasonably fast and not too complex algorithmically. It does the computations on tiles of 32x32 (default) pairs per workgroup, which reduces the number of times data has to be fetched from the global memory leading to better GPU utilization and hence performance. Further, by using an integer accumulator for the pair correlation we are able to use the built-in atomic increment function to get even more speed up.

### The input file should be formatted as shown below (see provided input.dat for a sample):

    box_start_x      box_end_x
    box_start_y      box_end_y
    box_start_z      box_end_z
    
    particle_diameter
    
    x1   y1   z1
    ...
    ...
    ...
    xN   yN   zN
   
### Run the program as follows:

`./ocl-pair-corr -in input_file -out output_file`

### If multiple OpenCL devices are present choose the required device as:

`./ocl-pair-corr -in input_file -out output_file -cl_dev device_num`
