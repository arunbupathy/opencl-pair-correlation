CXX = g++ -Wall
CC = gcc -Wall
OPFLAGS = -O2 -march=native
LDFLAGS = -lOpenCL -lm

all: ocl-pair-corr clean

ocl-pair-corr: ocl_create_programs.o ocl_pair_corr.o helper.o main.o
	$(CXX) $? $(LDFLAGS) -o $@

main.o: main.cpp
	$(CXX) $(OPFLAGS) $? -c

helper.o: helper.cpp
	$(CXX) $(OPFLAGS) $? -c

ocl_pair_corr.o: ocl_pair_corr.cpp
	$(CXX) $(OPFLAGS) $? -c

ocl_create_programs.o: ocl_create_programs.c
	$(CC) $(OPFLAGS) $? -c

clean:
	rm -f helper.o main.o ocl_create_programs.o ocl_pair_corr.o
